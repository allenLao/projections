from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import gensim
import numpy as np
import argparse

#https://github.com/rouseguy/DeepLearning-NLP

def load_model(path):
    model = gensim.models.KeyedVectors.load_word2vec_format(path)
    return model.wv

def closestwords(model, word, top_k=300):
    word_labels = [word]
    close_words = model.similar_by_word(word, top_k)
    tokens, embeds = [], []
    
    for wrd_score in close_words:
        embeds.append(model[wrd_score[0]])
        tokens.append(wrd_score[0])
    return (tokens, embeds)


def tsne_plot(embeddings, fout, perplexity=30, n_iter=5000, random_state=13):
    tokens, embeds = embeddings
    tsne_model = TSNE(perplexity=perplexity, n_components=2, init='pca', n_iter=n_iter, random_state=random_state)
    new_values = tsne_model.fit_transform(embeds)
    x, y = [], []
    for value in new_values:
        x.append(value[0])
        y.append(value[1])
        
    plt.figure(figsize=(16, 16)) 
    for i in range(len(x)):
        plt.scatter(x[i],y[i])
        plt.annotate(tokens[i],
                     xy=(x[i], y[i]),
                     xytext=(5, 2),
                     textcoords='offset points',
                     ha='right',
                     va='bottom')
    plt.savefig(fout)
    #plt.show()

def main(args):
    model = load_model(args.path)
    tokens, embeds = closestwords(model, args.word, args.topk)
    tsne_plot((tokens, embeds), args.fout, perplexity=args.perplexity, n_iter=args.n_iter)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default='word2vec.txt', help='input file')
    parser.add_argument('--word', type=str, default='relationship', help='seed words')
    parser.add_argument('--topk', type=int, default=300, help='dim of word embed')
    parser.add_argument('--perplexity', type=int, default=3, help='perplexity for the tsne')
    parser.add_argument('--n_iter', type=int, default=5000, help='number of iteration to compute pca')
    parser.add_argument('--fout', type=str, default='t-sne.png', help='output path to figures')
    args = parser.parse_args()
    main(args)
