from collections import Counter
import argparse
import pickle
# estimate word distribution
def WordDist(fin, fout):
    with open(fin, 'r', encoding='utf-8') as reader:
        counter = Counter()
        for line in reader:
            line = line.strip()
            if len(line) < 1: continue
            counter.update(line.split())
        #
        total = sum(counter.values())
        vocab = {}
        for k, v in counter.items():
            vocab[k] = 1.0 * v / total

        with open(fout, 'wb') as writer:
            pickle.dump(vocab, writer)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--text', help='input file')
    parser.add_argument('--lm', help='language model')
    args = parser.parse_args()
    text = args.text
    lm = args.lm
    WordDist(text, lm)