from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import numpy as np
import argparse

#https://github.com/rouseguy/DeepLearning-NLP

def load(path):
    """Loading word embeddingf files. 
       Format of each line: word val_0 val_1 ...
    """
    with open(path, 'r', encoding='utf-8') as reader:
        tokens, embeds = [], []
        header = reader.readline().strip().split()
        ntoken = int(header[0])
        dim = int(header[1])
        for line in reader:
            items = line.strip().split()
            tokens.append(' '.join(items[:len(items) - dim]))
            embeds.append(np.asarray([float(i) for i in items[-dim:]], dtype=np.float32))
        return tokens, embeds
    
def tsne_plot(embeddings, fout, perplexity=30, n_iter=5000, random_state=13):
    tokens, embeds = embeddings
    tsne_model = TSNE(perplexity=perplexity, n_components=2, init='pca', n_iter=n_iter, random_state=random_state)
    new_values = tsne_model.fit_transform(embeds)
    x, y = [], []
    for value in new_values:
        x.append(value[0])
        y.append(value[1])
        
    plt.figure(figsize=(16, 16)) 
    for i in range(len(x)):
        plt.scatter(x[i],y[i])
        plt.annotate(tokens[i],
                     xy=(x[i], y[i]),
                     xytext=(5, 2),
                     textcoords='offset points',
                     ha='right',
                     va='bottom')
    plt.savefig(fout)
    #plt.show()

def main(args):
    tokens, embeds = load(args.path)
    tsne_plot((tokens, embeds), args.fout, perplexity=args.perplexity, n_iter=args.n_iter)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', help='input file')
    parser.add_argument('--dim', help='dim of word embed')
    parser.add_argument('--perplexity', type=int, default=30, help='perplexity for the tsne')
    parser.add_argument('--n_iter', type=int, default=5000, help='number of iteration to compute pca')
    parser.add_argument('--fout', type=str, default='t-sne.png', help='output path to figures')
    args = parser.parse_args()
    main(args)
